from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from .forms import Input_Form
from .models import Course

# Create your views here.
def course(request):
    response = {
        'input_form': Input_Form,
        'courses': Course.objects.all()
    }
    return render(request, 'story5/course.html', response)

def details(request, id):
    course_obj = get_object_or_404(Course, pk=id)
    course_response = {
        'course': course_obj
    }
    return render(request, 'story5/details.html', course_response)

def delete(request, id):
    course = get_object_or_404(Course, pk=id)
    course.delete()
    messages.success(request, f'Course has been deleted!')
    return HttpResponseRedirect('/course')

def saveform(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        messages.success(request, f'Course added to list!')
    else:
        form = Input_Form()
        messages.warning(request, f'Credit must be between 1 and 24!')
    return HttpResponseRedirect('/course')