from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=30)
    lecturer = models.CharField(max_length=50)
    credit = models.IntegerField(
        validators=[
            MinValueValidator(1),
            MaxValueValidator(24)
        ]
    )
    desc = models.TextField()
    term = models.CharField(max_length=30)
    room = models.CharField(max_length=30)

    def __str__(self):
        return self.name