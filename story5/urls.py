from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.course, name="course"),
	path('saveform', views.saveform, name="saveform"),
	path('details/<int:id>', views.details, name="details"),
	path('delete/<int:id>', views.delete, name="delete"),
]