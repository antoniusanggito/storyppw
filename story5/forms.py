from django import forms
from .models import Course

class Input_Form(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'lecturer', 'credit', 'desc', 'term', 'room']

        name_attr = {
            'type': 'text',
            'placeholder': 'Course name',
        }
        lecturer_attr = {
            'type': 'text',
            'placeholder': 'Lecturer\'s name'
        }
        credit_attr = {
            'placeholder': 'Credits count'
        }
        desc_attr = {
            'type': 'text',
            'placeholder': 'Add description here . . .'
        }
        term_attr = {
            'type': 'text',
            'placeholder': 'ex. Gasal 2019/2020'
        }
        room_attr = {
            'type': 'text',
            'placeholder': 'Room location of the course'
        }

        widgets = {
            'name': forms.TextInput(attrs=name_attr),
            'lecturer': forms.TextInput(attrs=lecturer_attr),
            'credit': forms.NumberInput(attrs=credit_attr),
            'desc': forms.Textarea(attrs=desc_attr),
            'term': forms.TextInput(attrs=term_attr),
            'room': forms.TextInput(attrs=room_attr)
        }

        labels = {
            'name': 'Course',
            'lecturer': 'Lecturer',
            'credit': 'Credits',
            'desc': 'Description',
            'term': 'Term',
            'room': 'Room'
        }
