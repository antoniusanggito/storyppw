$(document).ready(function(){
    var index = 0;
    var total;

    $('#search-form').submit(function(e){
        e.preventDefault();
        index = 0;
        $.ajax({
            type: 'GET',
            data: {"query": $('input.text').val(), "start": index},
            url: "search/",
            success: function(response){
                index += 10;
                $('div.result').empty();
                total = response.totalItems;
                console.log(total);
                if (total == 0) {
                    $('#no-result').remove();
                    $('div.wrapper').append('<h2 id="no-result">There is no result</h2>');
                } else {
                    $('#no-result').remove();
                    $.each(response.items, function(i, val){
                        var title = val.volumeInfo.title
                        var author = val.volumeInfo.authors;
                        if (author == undefined) {
                            author = " - ";
                        } else if (author.length > 2) {
                            author = author[0] + ", " + author[1] + ", et al";
                        }
                        var image = val.volumeInfo.imageLinks ? val.volumeInfo.imageLinks.thumbnail : "";
                        var web_link = val.volumeInfo.infoLink;

                        var clone = $("#template-result").contents().clone(true);
                        clone.find('h4').text(title);
                        clone.find('h6').text("Author: " + author);
                        clone.find('img').attr("src", image);
                        clone.find('a').attr("href", web_link);
                        $('div.result').append(clone);
                    })
                }
            },
            error: function(response){
                alert("Something went wrong, please check your internet connection.")
            }
        })
    })

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if (index <= total) {
                $.ajax({
                    type: 'GET',
                    data: {"query": $('input.text').val(), "start": index},
                    url: "search/",
                    success: function(response){
                        index += 10;
                        if (response.totalItems == 0) {
                            $('#no-result').remove();
                            $('div.wrapper').append('<h2 id="no-result">There is no result</h2>');
                        } else {
                            $('#no-result').remove();
                            $.each(response.items, function(i, val){
                                var title = val.volumeInfo.title
                                var author = val.volumeInfo.authors;
                                if (author == undefined) {
                                    author = " - ";
                                } else if (author.length > 2) {
                                    author = author[0] + ", " + author[1] + ", et al";
                                }
                                var image = val.volumeInfo.imageLinks.thumbnail;
                                var web_link = val.volumeInfo.infoLink;
        
                                var clone = $("#template-result").contents().clone(true);
                                clone.find('h4').text(title);
                                clone.find('h6').text("Author: " + author);
                                clone.find('img').attr("src", image);
                                clone.find('a').attr("href", web_link);
                                $('div.result').append(clone);
                            })
                        }
                    },
                    error: function(response){
                        alert("Something went wrong, please check your internet connection.")
                    }
                })
            }
        }
     });
})
