from django.test import TestCase
import requests

#coverage run --include="story8/*" --omit="story1,story3,story5,story6,story7,manage.py,ppwstory" manage.py test

class MainTest(TestCase):
    def test_if_books_url_exists(self):
        response = self.client.get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_if_books_is_using_books_template(self):
        response = self.client.get('/books/')
        self.assertTemplateUsed(response, 'story8/books.html')

    def test_if_search_is_success(self):
        title = "frozen"
        response = self.client.get('/books/search/?query=' + title + '&start=0')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn("frozen", html_response)

    def test_if_search_redirects_with_post_request(self):
        title = "frozen"
        response = self.client.post('/books/search/?query=' + title + '&start=0')
        self.assertEqual(response.status_code, 302)