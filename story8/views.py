from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
import requests

def books(request):
    return render(request, 'story8/books.html')

def search(request):
    query = request.GET.get("query", None)
    start = request.GET.get("start", None)
    if request.method == "GET" and query != None:
        url = f"https://www.googleapis.com/books/v1/volumes?q={query}&startIndex={start}"
        response = requests.get(url).json()
        return JsonResponse(response, status=200)
    else:
        return HttpResponseRedirect('/books')