$(document).ready(function(){
    $(".btn-up").click(function() {
        var accordion_section = $(this).parent().parent().parent();
        var accordion_prev_section = $(accordion_section).prev();
        $(accordion_section).insertBefore($(accordion_prev_section));
    })

    $(".btn-down").click(function() {
        var accordion_section = $(this).parent().parent().parent();
        var accordion_next_section = $(accordion_section).next();
        $(accordion_section).insertAfter($(accordion_next_section));
    })

    $(".accordion-head").click(function(){
        var id = this.id;
    
        $(".accordion-content").each(function(){
            if($("#"+id).next()[0].id != this.id){
                $(this).slideUp("medium");
            }
        });
    
        $(this).next().toggle("medium");
    });
});