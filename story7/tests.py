from django.test import TestCase, Client

# coverage run --include="story7/*" --omit="story1,story3,story5,story6,manage.py,ppwstory" manage.py test
class MainTest(TestCase):
    def test_moreinfo_is_exist(self):
        response = Client().get('/moreinfo/')
        self.assertEqual(response.status_code, 200)

    def test_moreinfo_is_using_experience_template(self):
        response = Client().get('/moreinfo/')
        self.assertTemplateUsed(response, 'story7/moreinfo.html')