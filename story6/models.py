from django.db import models

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=30)
    desc = models.TextField()

    def __str__(self):
        return self.name

class Person(models.Model):
    name = models.CharField(max_length=30)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

    def __str__(self):
        return self.name