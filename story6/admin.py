from django.contrib import admin
from .models import Activity, Person

# Register your models here.
admin.site.register(Activity)
admin.site.register(Person)