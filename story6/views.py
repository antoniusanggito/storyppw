from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from .forms import Input_Form, Person_Form
from .models import Activity, Person

# Create your views here.
def activity(request):
    response = {
        'input_form': Input_Form,
        'activity_list': Activity.objects.all(),
        'person_list': Person.objects.all(),
        'person_form': Person_Form
    }
    return render(request, 'story6/activity.html', response)

def save_activity(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        value = request.POST['name']
        messages.success(request, f'Activity {value} has been added!')
    else:
        form = Input_Form()
    return HttpResponseRedirect('/activity#activity')

def add_person(request, id):
    form = Person_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        value = request.POST['name']
        activity_obj = get_object_or_404(Activity, pk=id)
        Person.objects.create(name=value, activity=activity_obj)
        messages.success(request, f'{value} has been added to {activity_obj.name}.')
    return HttpResponseRedirect('/activity#activity')

def delete_activity(request, id):
    if (request.method == 'POST'):
        activity_obj = get_object_or_404(Activity, pk=id)
        messages.success(request, f'Activity {activity_obj} has been deleted!')
        activity_obj.delete()
    return HttpResponseRedirect('/activity#activity')

def delete_person(request, id):
    if (request.method == 'POST'):
        person_obj = get_object_or_404(Person, pk=id)
        messages.success(request, f'{person_obj} has been deleted from {person_obj.activity}.')
        person_obj.delete()
    return HttpResponseRedirect('/activity#activity')