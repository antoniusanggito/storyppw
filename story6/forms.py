from django import forms
from .models import Activity, Person

class Input_Form(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['name', 'desc']

        name_attr = {
            'type': 'text',
            'placeholder': 'Activity name',
        }
        desc_attr = {
            'type': 'text',
            'placeholder': 'Add description here . . .'
        }

        labels = {
            'name': 'Activity',
            'desc': 'Description'
        }

        widgets = {
            'name': forms.TextInput(attrs=name_attr),
            'desc': forms.Textarea(attrs=desc_attr)
        }


class Person_Form(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name']

        labels = {
            'name': ''
        }

        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Your name', 'class': 'person-add'})
        }