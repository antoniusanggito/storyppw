from django.test import TestCase, Client
from .models import Activity, Person

# Create your tests here.
# coverage run --include="story6/*" --omit="story1,story3,story5,manage.py,ppwstory" manage.py test

class MainTestCase(TestCase):
    def test_activity_is_exist_and_using_activity_template(self):
        response = Client().get('/activity/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/activity.html')

    def test_when_save_activity_is_valid(self):
        response = Client().post('/activity/save_activity', {'name': 'UnitTest', 'desc': 'Testing'})
        self.assertEqual(Activity.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn("UnitTest", html_response)

    def test_when_save_activity_is_not_valid(self):
        response = Client().post('/activity/save_activity', {})
        self.assertEqual(Activity.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)

    def test_add_person_is_valid(self):
        activity_id = Activity.objects.create(name="UnitTest", desc="Testing").id
        response = Client().post('/activity/add_person/' + str(activity_id), {'name': 'Budi', 'activity': Activity.objects.get(id=activity_id)})
        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn("Budi", html_response)

    def test_delete_activity_is_valid(self):
        activity_id = Activity.objects.create(name="UnitTest", desc="Testing").id
        self.assertEqual(Activity.objects.all().count(), 1)
        response = Client().post('/activity/delete_activity/' + str(activity_id), {})
        self.assertEqual(Activity.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)
        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("UnitTest", html_response)

    def test_delete_person_is_valid(self):
        activity_id = Activity.objects.create(name="UnitTest", desc="Testing").id
        person_id = Person.objects.create(name="Budi", activity=Activity.objects.get(id=activity_id)).id
        self.assertEqual(Person.objects.all().count(), 1)
        response = Client().post('/activity/delete_person/' + str(person_id), {})
        self.assertEqual(Person.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)
        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Budi", html_response)