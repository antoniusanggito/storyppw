from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.activity, name="activity"),
    path('save_activity', views.save_activity, name="save_activity"),
    path('add_person/<int:id>', views.add_person, name="add_person"),
    path('delete_activity/<int:id>', views.delete_activity, name="delete_activity"),
    path('delete_person/<int:id>', views.delete_person, name="delete_person"),
]