# ppwstory

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Repositori ini berisi tugas Story PPW Antonius Anggito Arissaputro (1906292912) menggunakan framework Django yang siap
di-*deploy* ke Heroku melalui GitLab CI di URL antoniusanggito-story.herokuapp.com


[pipeline-badge]: https://gitlab.com/antoniusanggito/storyppw/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/antoniusanggito/storyppw/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/antoniusanggito/storyppw/-/commits/master