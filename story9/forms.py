from django import forms
from .models import MyUser

class CustomUserCreationForm(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ('email', 'username','password1', 'password2')

    email = forms.EmailField(label="", widget=forms.EmailInput(
        attrs = {
                "id" : "email_sign",
                "class" : "form-input",
                "placeholder" : "Email",
                }
    ))
    username = forms.SlugField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "username_sign",
                "class" : "form-input",
                "placeholder" : "Username",
                }
    ))
    password1 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_1",
                "class" : "form-input",
                "placeholder" : "Password",
                }
    ))
    password2 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_2",
                "class" : "form-input",
                "placeholder" : "Konfirmasi Password",
                }
    ))

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class CustomAuthenticationForm(forms.Form):
    # username = forms.SlugField(label="", widget=forms.TextInput(
    #     attrs = {
    #             "id" : "username_sign",
    #             "class" : "form-input",
    #             "placeholder" : "Username",
    #             }
    # ))
    email = forms.EmailField(label="", widget=forms.EmailInput(
        attrs = {
                "id" : "email_sign",
                "class" : "form-input",
                "placeholder" : "Email",
                }
    ))
    password = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "password_sign",
                "class" : "form-input",
                "placeholder" : "Password",
                "autocomplete" : "true",
                }
    ))

    class Meta:
        fields = ['email', 'password']