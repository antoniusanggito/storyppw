from django.test import TestCase
from .models import MyUser

#coverage run --include="story9/*" --omit="story1,story3,story5,story6,story7,story8,manage.py,ppwstory" manage.py test

class MainTest(TestCase):
    def test_if_dashboard_is_exists(self):
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/dashboard.html')

    def test_if_login_is_exists(self):
        response = self.client.get('/dashboard/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_if_register_is_exists(self):
        response = self.client.get('/dashboard/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/register.html')

    def test_register_is_success(self):
        response = self.client.post('/dashboard/register/', {"email": "test@gmail.com", 'username': "test", "password1": "test123", "password2": "test123"})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(MyUser.objects.all().count(), 1)
        # self.client.login(username="test@gmail.com", password="test123")
        # response = self.client.get('/dashboard/')
        # self.assertIn('test', response.content.decode('utf8'))
        # self.assertEqual(response.status_code, 200)

    def test_login_is_success(self):
        user = MyUser.objects.create(email="test@gmail.com", username="test")
        user.set_password('test123')
        user.save()
        response = self.client.post('/dashboard/login/', {'email': "test@gmail.com", "password": "test123"})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/dashboard/')
        self.assertIn('test', response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)

    def test_logout_is_success(self):
        user = MyUser.objects.create(email="test@gmail.com", username="test")
        user.set_password('test123')
        user.save()
        self.client.login(username="test@gmail.com", password="test123")
        response = self.client.get('/dashboard/logout/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/dashboard/')
        self.assertNotIn('test', response.content.decode('utf8'))