from django.contrib import admin
from .models import MyUser
from django.contrib.auth.models import Group

admin.site.register(MyUser)

admin.site.unregister(Group)