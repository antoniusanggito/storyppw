from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.dashboard, name="dashboard"),
    path('register/', views.register, name="register"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
]