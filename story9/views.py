from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login, authenticate as django_authenticate, logout as django_logout
from .forms import CustomUserCreationForm, CustomAuthenticationForm

def dashboard(request):
    return render(request, 'story9/dashboard.html')

def register(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/dashboard')
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            raw_password = form.cleaned_data['password1']
            user = django_authenticate(username=email, password=raw_password)
            django_login(request, user)
            return HttpResponseRedirect('/dashboard')
    else:
        form = CustomUserCreationForm()
    return render(request, 'story9/register.html', {'form': form})

def login(request):
    messages = []
    if request.user.is_authenticated == True:
        return HttpResponseRedirect('/dashboard')
    if request.method == 'POST':
        form = CustomAuthenticationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = django_authenticate(username=email, password=password)
            if user:
                if user.is_active:
                    django_login(request, user)
                    return HttpResponseRedirect('/dashboard')
            messages.append('Email atau password salah!')
    else:
        form = CustomAuthenticationForm()
    return render(request, 'story9/login.html', {'form': form, 'messages': messages})
    
@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect('/dashboard')